# frozen_string_literal: true

require_relative '../lib/missing_milestone_helper'

Gitlab::Triage::Resource::Context.include MissingMilestoneHelper
