# frozen_string_literal: true

require 'active_support'
require 'active_support/core_ext/numeric/time'

require_relative 'devops_labels'
require_relative 'versioned_milestone'
require_relative 'constants/slo_targets'
require_relative 'constants/labels'
require_relative 'shared/resource_notes_helper'
require_relative 'shared/current_group_helper'

module SloBreachHelper
  include DevopsLabels::Context
  include CurrentGroupHelper
  include ResourceNotesHelper

  SEVERITY_LABELS = {
    s1: 'severity::1',
    s2: 'severity::2',
    s3: 'severity::3',
    s4: 'severity::4'
  }.freeze

  ENGINEERING_MANAGER = 'Engineering Manager'
  PRODUCT_MANAGER = 'Product Manager'
  SOFTWARE_ENGINEER_IN_TEST = 'Software Engineer in Test'
  FEDRAMP_VULNERABILITY_LABEL = Labels::FEDRAMP_VULNERABILITY_LABEL
  VULNERABILITY_SLA_LABEL = Labels::VULNERABILITY_SLA_LABEL

  SLO_WARNING_PERCENTAGE = 50.0

  COMMUNITY_BACKUP_DRI = '@gitlab-com/quality/contributor-success'

  def current_severity_label
    (label_names & SEVERITY_LABELS.values).first
  end

  def slo_breach_looming?
    return false unless severity_label_with_details
    return false if versioned_milestone.current?(milestone)

    warning_date = severity_label_added_date + slo_breach_warning_threshold(current_severity_label).days
    warning_date < today
  end

  def days_til_slo_breach_with_breach_date
    "#{days_til_breach} days (#{breach_date.iso8601})"
  end

  def days_til_breach
    (breach_date - today).to_i
  end

  def overdue?
    return false unless severity_label_with_details
    return false unless milestone&.due_date

    breach_date < milestone.due_date
  end

  def community_team_escalation
    current_group_em || COMMUNITY_BACKUP_DRI
  end

  def responsible_em
    current_group_em || "~\"#{current_group_label}\" #{ENGINEERING_MANAGER}"
  end

  def responsible_pm
    current_group_pm || "~\"#{current_group_label}\" #{PRODUCT_MANAGER}"
  end

  def responsible_set
    current_group_set || "~\"#{current_group_label}\" #{SOFTWARE_ENGINEER_IN_TEST}"
  end

  def milestones_count_before_breach_slo?
    return 0 unless days_til_breach.positive?

    return nil unless milestone_before_breach

    versioned_milestone.all_non_expired.index do |milestone|
      milestone.title == milestone_before_breach.title
    end + 1
  end

  def slo_target_for_severity
    all_slo_targets[current_severity_label]
  end

  def vulnerability_label
    labels_with_details.find do |label|
      label.name == FEDRAMP_VULNERABILITY_LABEL || label.name == VULNERABILITY_SLA_LABEL
    end
  end

  def milestone_before_breach
    return nil unless breach_date

    versioned_milestone.find_milestone_for_date(breach_date - 1)
  end

  def today
    @today ||= Date.today
  end

  def markdown_label(label_name)
    %(~"#{label_name}")
  end

  private

  def versioned_milestone
    @versioned_milestone ||= VersionedMilestone.new(self)
  end

  def all_slo_targets
    if vulnerability_label
      SloTargets::VULNERABILITY_SEVERITY_SLO_TARGETS
    else
      SloTargets::DEFAULT_SEVERITY_SLO_TARGETS
    end
  end

  def severity_label_with_details
    @severity_label_with_details ||= labels_with_details.find do |label|
      all_slo_targets.key?(label.name)
    end
  end

  def severity_label_added_date
    return nil unless severity_label_with_details&.added_at

    severity_label_with_details.added_at.to_date
  end

  def breach_date
    start_date = severity_label_added_date || resource[:created_at].to_datetime
    start_date + slo_target_for_severity.days
  end

  def slo_breach_warning_threshold(severity_label)
    slo_target = all_slo_targets[severity_label]
    slo_target * (SLO_WARNING_PERCENTAGE / 100)
  end
end
