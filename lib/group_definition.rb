# frozen_string_literal: true

require 'yaml'

require_relative './www_gitlab_com'

module GroupDefinition
  module_function

  STAGE_DATA = WwwGitLabCom.stages
  DATA = YAML.load_file(File.expand_path("#{__dir__}/../group-definition.yml")).freeze

  DEFAULT_ASSIGNEE_FIELDS = %w[
    pm
    engineering_manager
    backend_engineering_manager
    frontend_engineering_manager
    extra_assignees
  ].freeze

  DATA.each do |name, definition|
    group_method_name = "group_#{name}".tr('-', '_')

    define_method(group_method_name) do |fields = nil|
      assignee_fields =
        if fields
          fields & DEFAULT_ASSIGNEE_FIELDS
        else
          DEFAULT_ASSIGNEE_FIELDS
        end

      stage_definition = STAGE_DATA.find { |stage, definition| definition['groups'].include?(name) }
      stage = if definition['stage_label']
                { 'label' => definition['stage_label'] }
              elsif stage_definition
                stage_definition[1]
              else
                {}
              end

      result = {
        assignees:
          definition.values_at(*assignee_fields).flatten.compact.uniq,
        labels:
          [definition.fetch('group_label', "group::#{name.tr('_', ' ')}")] + definition.fetch('extra_labels', []),
        stage: stage,
        default_labeling: definition.fetch('default_labeling', {})
      }

      if (fields.nil? || fields.include?('extra_mentions')) && extra_mentions = definition['extra_mentions']
        result[:mentions] = (result[:assignees] + extra_mentions).uniq
      end

      result
    end
  end

  def pm_for_team(group)
    DATA.dig(group, 'pm')&.first
  end

  def em_for_team(group, scope = :backend)
    key = case scope
          when :backend
            'backend_engineering_manager'
          else
            'frontend_engineering_manager'
          end
    (DATA.dig(group, key) || DATA.dig(group, 'engineering_manager'))&.first
  end
end
