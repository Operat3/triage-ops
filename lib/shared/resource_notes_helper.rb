# frozen_string_literal: true

require_relative '../../triage/triage'

module ResourceNotesHelper
  def has_unique_comment?(unique_comment_string, author = Triage::Event::GITLAB_BOT_USERNAME)
    resource_notes.any? do |note|
      note.to_h.dig('author', 'username') == author &&
        note.body.include?(unique_comment_string)
    end
  end

  def last_reviewer
    unassignment_body_regexp = /removed review request for @(.+?)($|,|\s)/.freeze

    user = resource_notes
      .select { |note| note.body =~ unassignment_body_regexp }
      .max_by { |note| Time.parse(note.created_at) }
      &.body
      &.match(unassignment_body_regexp)
      &.captures
      &.first

    return nil if user.nil?

    "@#{user}"
  end

  private

  def project_id
    resource[:project_id]
  end

  def resource_iid
    resource[:iid]
  end

  def resource_notes
    path = resource[:type] == 'issues' ? 'issue_notes' : 'merge_request_notes'
    @resource_notes ||= Triage.api_client.public_send(path, project_id, resource_iid, per_page: 100).auto_paginate
  end
end
