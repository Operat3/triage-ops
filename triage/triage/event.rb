# frozen_string_literal: true

require 'time'

require_relative '../triage'
require_relative 'error'
require_relative '../resources/user'
require_relative 'part_of_product_projects'
require_relative '../../lib/team_member_select_helper'

module Triage
  class Event
    ObjectKindNotProvidedError = Class.new(Triage::ClientError)
    UnknownObjectKind = Class.new(Triage::ClientError)

    # https://gitlab.com/gitlab-dependency-update-bot
    GITLAB_RENOVATE_BOT_ID = 8420142
    # https://gitlab.com/gitlab-release-tools-bot
    GITLAB_RELEASE_TOOLS_BOT_ID = 2324599
    # https://gitlab.com/gitlab-qa
    GITLAB_QA_BOT_ID = 1614863
    # https://gitlab.com/gitlab-bot
    GITLAB_BOT_ID = 1786152
    # https://gitlab.com/employment-bot
    GITLAB_EMPLOYMENT_BOT_ID = 3927967
    # https://gitlab.com/gitlab-dependency-bot
    GITLAB_DEPENDENCY_BOT_ID = 3110502

    GITLAB_BOT_USERNAME = 'gitlab-bot'

    # Automation users that are not part of the GitLab groups for security reasons
    AUTOMATION_IDS = [
      GITLAB_BOT_ID,
      GITLAB_DEPENDENCY_BOT_ID,
      GITLAB_EMPLOYMENT_BOT_ID,
      GITLAB_QA_BOT_ID,
      GITLAB_RELEASE_TOOLS_BOT_ID,
      GITLAB_RENOVATE_BOT_ID
    ].freeze
    GITLAB_SERVICE_ACCOUNT_REGEX = /^(project|group)_\d+_bot(\d+)?$/.freeze

    ISSUE = 'issue'
    INCIDENT = 'incident'
    MERGE_REQUEST = 'merge_request'
    NOTE = 'note'

    GITLAB_DESIGN_PROJECT_ID          = 443787
    GITLAB_PROJECT_ID                 = 278964
    MASTER_BROKEN_INCIDENT_PROJECT_ID = 40549124
    RUNBOOKS_PROJECT_ID               = 1148549
    TRIAGE_OPS_PLAYGROUND_PROJECT_ID  = 36072369
    WWW_GITLAB_COM_PROJECT_ID         = 7764
    TF_PROVIDER_GITLAB_PROJECT_ID     = 40916776

    attr_reader :payload

    def initialize(payload)
      @payload = payload
    end

    def self.build(payload)
      payload['object_kind'].then do |kind|
        raise ObjectKindNotProvidedError, '`object_kind` not provided!' if kind.nil?

        case kind
        when ISSUE
          if payload['object_attributes'].key?('escalation_status')
            IncidentEvent.new(payload)
          else
            IssueEvent.new(payload)
          end
        when MERGE_REQUEST
          MergeRequestEvent.new(payload)
        when NOTE
          NoteEvent.new(payload)
        else
          raise UnknownObjectKind, "Unknown object: #{kind.inspect}"
        end
      end
    end

    def key
      "#{object_kind}.#{action}"
    end

    def issue?
      object_kind == ISSUE
    end

    def incident?
      object_kind == INCIDENT
    end

    def merge_request?
      object_kind == MERGE_REQUEST
    end

    def note?
      object_kind == NOTE
    end

    def event_actor
      User.new(payload.fetch('user', {}))
    end

    def event_actor_id
      event_actor['id']
    end

    def event_actor_username
      event_actor['username']
    end

    def resource_author_id
      payload.dig('object_attributes', 'author_id')
    end

    def resource_author
      @resource_author ||= User.new(Triage.user(resource_author_id))
    end

    def by_resource_author?
      event_actor_id == resource_author_id
    end

    def team_member_author?
      TeamMemberSelectHelper.team_member_exist?(resource_author.username)
    end

    def by_team_member?
      TeamMemberSelectHelper.team_member_exist?(event_actor.username)
    end

    def created_at
      @created_at ||= Time.parse(payload.dig('object_attributes', 'created_at'))
    end

    def url
      @url ||= payload.dig('object_attributes', 'url')
    end

    def title
      @title ||= payload.dig('object_attributes', 'title')
    end

    def description
      @description ||= payload.dig('object_attributes', 'description').to_s
    end

    def action
      @action ||= payload.dig('object_attributes', 'action')
    end

    def milestone_id
      @milestone_id ||= payload.dig('object_attributes', 'milestone_id')
    end

    def added_milestone_id
      payload.dig('changes', 'milestone_id', 'current')
    end

    def new_entity?
      action == 'open'
    end

    def added_label_names
      @added_label_names ||= current_label_names - previous_label_names
    end

    def removed_label_names
      @removed_label_names ||= previous_label_names - current_label_names
    end

    def label_names
      @label_names ||= labels.map { |label_data| label_data['title'] }
    end

    def type_label
      @type_label ||= label_names.grep(/^type::/)&.first
    end

    def type_label_set?
      !type_label.nil?
    end

    def noteable_path
      @noteable_path ||= "/projects/#{project_id}/#{noteable_kind}/#{iid}"
    end

    def from_gitlab_org?(sandbox: false)
      if sandbox
        with_project_id?(TRIAGE_OPS_PLAYGROUND_PROJECT_ID)
      else
        event_from_group?(Triage::GITLAB_ORG_GROUP)
      end
    end

    def from_gitlab_org_security?
      event_from_group?(Triage::GITLAB_ORG_SECURITY_GROUP)
    end

    def from_gitlab_com?(sandbox: false)
      if sandbox
        with_project_id?(TRIAGE_OPS_PLAYGROUND_PROJECT_ID)
      else
        event_from_group?(Triage::GITLAB_COM_GROUP)
      end
    end

    def from_gitlab_org_gitlab?
      with_project_id?(GITLAB_PROJECT_ID)
    end

    def from_www_gitlab_com?
      with_project_id?(WWW_GITLAB_COM_PROJECT_ID)
    end

    def from_runbooks?
      with_project_id?(RUNBOOKS_PROJECT_ID)
    end

    def from_part_of_product_project?
      return @from_part_of_product_project if defined?(@from_part_of_product_project)

      # TODO: Also check against ops.gitlab.net projects? For now we only have webhooks for GitLab.com...
      found_project = Triage::PartOfProductProjects.part_of_product_projects(:com).find do |row|
        with_project_id?(row['project_id'])
      end

      @from_part_of_product_project = !found_project.nil?
    end

    def from_master_broken_incidents_project?
      with_project_id?(MASTER_BROKEN_INCIDENT_PROJECT_ID)
    end

    def automation_author?
      AUTOMATION_IDS.include?(resource_author_id)
    end

    def gitlab_bot_event_actor?
      event_actor_username == GITLAB_BOT_USERNAME
    end

    def author_is_gitlab_service_account?
      resource_author.username.match?(GITLAB_SERVICE_ACCOUNT_REGEX)
    end

    def wider_community_author?
      !automation_author? &&
        !author_is_gitlab_service_account? &&
        !team_member_author?
    end

    def jihu_contributor?
      Triage.jihu_team_member_ids.include?(resource_author_id)
    end

    def with_project_id?(expected_project_id)
      project_id == expected_project_id.to_i
    end

    def project_id
      @project_id ||= payload.dig('project', 'id')
    end

    def project_web_url
      @project_web_url ||= payload.dig('project', 'web_url')
    end

    def project_path_with_namespace
      @project_path_with_namespace ||= payload.dig('project', 'path_with_namespace')
    end

    def object_kind
      @object_kind ||= payload.fetch('object_kind', 'unknown')
    end

    def assignee_ids
      payload.dig('object_attributes', 'assignee_ids') || []
    end

    def resource_open?
      state == 'opened'
    end

    def noteable_kind
      raise NotImplementedError
    end

    def iid
      raise NotImplementedError
    end

    def project_public?
      project_visibility_level == 20
    end

    def project_internal?
      project_visibility_level == 10
    end

    def project_private?
      project_visibility_level.zero?
    end

    private

    def state
      raise NotImplementedError
    end

    def project_visibility_level
      raise NotImplementedError
    end

    def changes
      payload.fetch('changes', {})
    end

    def labels
      payload.fetch('labels', [])
    end

    def previous_labels
      changes.dig('labels', 'previous') || []
    end

    def current_labels
      if new_entity?
        labels
      else
        changes.dig('labels', 'current') || []
      end
    end

    def previous_label_names
      previous_labels.map { |l| l['title'] }
    end

    def current_label_names
      current_labels.map { |l| l['title'] }
    end

    def event_from_group?(group)
      %r{\A#{group}/}.match?(payload.dig('project', 'path_with_namespace'))
    end
  end

  class IssuableEvent < Event
    def iid
      @iid ||= payload.dig('object_attributes', 'iid')
    end

    private

    def state
      payload.dig('object_attributes', 'state')
    end

    def noteable_kind
      raise NotImplementedError
    end
  end

  class IssueEvent < IssuableEvent
    def weight
      @weight ||= payload.dig('object_attributes', 'weight')
    end

    private

    def noteable_kind
      'issues'
    end

    def project_visibility_level
      payload.dig('project', 'visibility_level')
    end
  end

  class IncidentEvent < IssuableEvent
    def object_kind
      @object_kind ||= INCIDENT
    end
  end

  class MergeRequestEvent < IssuableEvent
    STABLE_BRANCH_SUFFIX = 'stable-ee'

    def source_branch
      payload.dig('object_attributes', 'source_branch')
    end

    def target_branch
      payload.dig('object_attributes', 'target_branch')
    end

    def wip?
      payload.dig('object_attributes', 'work_in_progress')
    end

    def merge_event?
      action == 'merge'
    end

    def revision_update?
      # When oldrev exists then it's updating revision
      # https://gitlab.com/gitlab-org/gitlab/-/blob/438638cf7522ee409962fe127cff6a042584655a/app/services/merge_requests/refresh_service.rb#L275
      # https://gitlab.com/gitlab-org/gitlab/-/blob/438638cf7522ee409962fe127cff6a042584655a/app/services/merge_requests/base_service.rb#L16
      !!(action == 'update' && payload.dig('object_attributes', 'oldrev'))
    end

    def approval_event?
      action == 'approval'
    end

    def approved_event?
      action == 'approved'
    end

    def source_branch_is?(branch)
      source_branch == branch
    end

    def target_branch_is_main_or_master?
      branch = target_branch

      branch == 'main' || branch == 'master'
    end

    def target_branch_is_stable_branch?
      target_branch.end_with?(STABLE_BRANCH_SUFFIX)
    end

    def last_commit_sha
      payload.dig('object_attributes', 'last_commit', 'id')
    end

    def revert_mr?
      title.start_with?(/[Rr]evert/)
    end

    private

    def noteable_kind
      'merge_requests'
    end

    def project_visibility_level
      payload.dig('object_attributes', 'target', 'visibility_level')
    end
  end

  class NoteEvent < Event
    UnknownNoteableTypeError = Class.new(Triage::ClientError)

    def iid
      @iid ||= payload.dig(noteable_type, 'iid')
    end

    def key
      @key ||= "#{noteable_type}.#{object_kind}"
    end

    alias_method :new_comment, :description

    def resource_author_id
      @resource_author_id ||= payload.dig(noteable_type, 'author_id')
    end

    def noteable_author
      @noteable_author ||= User.new(Triage.user(resource_author_id))
    end

    def note_on_issue?
      noteable_type == ISSUE
    end

    def note_on_merge_request?
      noteable_type == MERGE_REQUEST
    end

    def noteable_type
      @noteable_type ||= to_snake_case(payload.dig('object_attributes', 'noteable_type'))
    end

    def assignee_ids
      case noteable_type
      when 'issue', 'merge_request'
        payload.dig(noteable_type, 'assignee_ids') || []
      else
        raise UnknownNoteableTypeError, "`#{noteable_type}` is an unknown noteable_type!"
      end
    end

    def title
      @title ||= payload.dig(noteable_type, 'title')
    end

    private

    def labels
      case noteable_type
      when 'issue', 'merge_request'
        payload.dig(noteable_type, 'labels') || []
      else
        raise UnknownNoteableTypeError, "`#{noteable_type}` is an unknown noteable_type!"
      end
    end

    def state
      payload.dig(noteable_type, 'state')
    end

    def project_visibility_level
      case noteable_type
      when 'issue'
        payload.dig('project', 'visibility_level')
      when 'merge_request'
        payload.dig('merge_request', 'target', 'visibility_level')
      else
        raise UnknownNoteableTypeError, "`#{noteable_type}` is an unknown noteable_type!"
      end
    end

    def noteable_kind
      "#{noteable_type}s"
    end

    def to_snake_case(string)
      string.gsub(/(?<=[a-z])[A-Z]/, '_\0').downcase
    end

    def noteable_from_api
      @noteable_from_api ||= Triage.api_client.public_send(noteable_type, project_id, iid)
    end
  end
end
