# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/processor'
require_relative '../triage/unique_comment'

require 'digest'
require 'slack-messenger'

module Triage
  class UxMrs < Processor
    UX_LABEL = 'UX'
    SLACK_CHANNEL = '#ux-mrs'
    SLACK_ICON = ':robot_face:'
    SLACK_MESSAGE_TEMPLATE = <<~MESSAGE
      Hi UX team, an MR is ready for UX review: (%<mr_title>s) - %<mr_url>s.
    MESSAGE

    react_to 'merge_request.open', 'merge_request.update'

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      event.from_gitlab_org? &&
        event.resource_open? &&
        !event.wip? &&
        event.team_member_author? &&
        ux_label_added? &&
        unique_comment.no_previous_comment?
    end

    def process
      send_review_request && post_ux_comment
    end

    def slack_options
      {
        channel: SLACK_CHANNEL,
        username: GITLAB_BOT,
        icon_emoji: SLACK_ICON
      }
    end

    def documentation
      <<~TEXT
        This processor posts reminder comment in UX merge requests to assigned reviewer suggested by Reviewer Roulette,
        and also pings the UX team slack channel about the merge request.
      TEXT
    end

    private

    attr_reader :messenger

    def ux_label_added?
      event.added_label_names.include?(UX_LABEL)
    end

    def send_review_request
      slack_message = format(SLACK_MESSAGE_TEMPLATE, mr_url: event.url, mr_title: event.title)
      messenger.ping(slack_message)

      true
    end

    def post_ux_comment
      add_comment(message.strip, append_source_link: true)
    end

    def message
      comment = <<~MESSAGE
        Please wait for Reviewer Roulette to suggest a designer for UX review, and then assign them as Reviewer. This helps evenly distribute reviews across UX.
      MESSAGE

      unique_comment.wrap(comment)
    end

    def slack_messenger
      Slack::Messenger.new(ENV.fetch('SLACK_WEBHOOK_URL', nil), slack_options)
    end
  end
end
