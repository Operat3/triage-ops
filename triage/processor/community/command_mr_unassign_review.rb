# frozen_string_literal: true

require 'digest'

require_relative 'community_processor'

require_relative '../../triage/rate_limit'

module Triage
  # Allows any community contributor to ask for a review.
  # The automation will pick a random MR coach and ask them to review the MR.
  class CommandMrUnassignReview < CommunityProcessor
    include RateLimit

    react_to 'merge_request.note'
    define_command name: 'unassign_review'

    def applicable?
      command.valid?(event) && user_is_reviewer?
    end

    def process
      comment = <<~MARKDOWN.chomp
        #{reviewer_username_action}
      MARKDOWN
      add_comment(comment, append_source_link: false)
    end

    def documentation
      <<~TEXT
      This allows reviewers to unassign themselves from being a reviewer of the MR.

      The command can be used as `@gitlab-bot unassign_review`.
      TEXT
    end

    private

    def user_is_reviewer?
      current_reviewers.include?(event.event_actor_username)
    end

    def reviewer_username_action
      "/remove_reviewer @#{event.event_actor_username}"
    end

    def current_reviewers
      merge_request.reviewers.map { |reviewer| reviewer['username'] }
    end

    def merge_request
      Triage.api_client.merge_request(event.project_id, event.iid)
    end

    def cache_key
      @cache_key ||= OpenSSL::Digest.hexdigest('SHA256', "unassign_review-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      100
    end

    def rate_limit_period
      3600 # 1 hour
    end
  end
end
