# frozen_string_literal: true

require 'digest'

require_relative 'community_processor'

require_relative '../../triage'
require_relative '../../triage/rate_limit'

module Triage
  class CommandMrLabel < CommunityProcessor
    include RateLimit

    LABELS_REGEX = /~"([^"]+)"|~([^ ]+)/.freeze
    ALLOWED_LABEL_SCOPES = %w[bug feature group maintenance type].freeze
    ALLOWED_LABELS_REGEX = /\A(#{ALLOWED_LABEL_SCOPES.join('|')})::[^:]+\z/.freeze
    CATEGORY_LABELS_REGEX = /\A(Category):[^:]+\z/.freeze
    EXTRA_ALLOWED_LABELS = [
      'backend',
      'database',
      'documentation',
      'frontend',
      'security',
      'UX',
      'workflow::in dev',
      'workflow::ready for review',
      'workflow::blocked',
      'automation:ml wrong'
    ].freeze

    react_to 'issue.note', 'merge_request.note'
    define_command name: 'label', args_regex: LABELS_REGEX

    def applicable?
      valid_command? && any_labels_to_apply?
    end

    def process
      post_label_command
    rescue Gitlab::Error::BadRequest => e
      # Ignore errors when the label doesn't exist. We could also post a note to the author to tell them that the label doesn't exist...
      return if e.response_status == 400 && e.response_message.include?(%(:note=>["can't be blank"]))

      raise e
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("reactive-labeler-commands-sent-#{event.event_actor_id}")
    end

    def documentation
      <<~TEXT
        This processor supports the "@gitlab-bot label" triage operation command to add labels to issues and merge requests.
      TEXT
    end

    private

    def any_labels_to_apply?
      labels_to_apply.any?
    end

    def labels_to_apply
      @labels_to_apply ||= command.args(event).select do |label|
        label.match?(ALLOWED_LABELS_REGEX) || label.match?(CATEGORY_LABELS_REGEX) || EXTRA_ALLOWED_LABELS.include?(label)
      end
    end

    def command_labels
      labels_to_apply.map { |label| %(~"#{label}") }.join(' ')
    end

    def post_label_command
      comment = <<~MARKDOWN.chomp
        /label #{command_labels}
      MARKDOWN
      add_comment(comment, append_source_link: false)
    end
  end
end
