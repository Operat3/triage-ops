# frozen_string_literal: true

require 'slack-messenger'

require_relative 'community_processor'

module Triage
  class MergedMrNotification < CommunityProcessor
    SLACK_CHANNEL = '#mr-feedback'
    SLACK_ICON = ':rocket:'
    SLACK_MESSAGE_TEMPLATE = <<~MESSAGE
      `@%<username>s`'s merge request %<mr_title>s was merged.
      %<mr_url>s
    MESSAGE

    react_to 'merge_request.merge'

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      wider_community_contribution?
    end

    def process
      post_merged_mr_to_slack
    end

    def slack_options
      {
        channel: SLACK_CHANNEL,
        username: GITLAB_BOT,
        icon_emoji: SLACK_ICON
      }
    end

    def documentation
      <<~TEXT
        This processor drops a note in slack when a community MR is merged.
      TEXT
    end

    private

    attr_reader :messenger

    def post_merged_mr_to_slack
      message = format(SLACK_MESSAGE_TEMPLATE, mr_url: event.url, mr_title: event.title, username: event.noteable_author.username)
      messenger.ping(text: message)
    end

    def slack_messenger
      Slack::Messenger.new(ENV.fetch('SLACK_WEBHOOK_URL', nil), slack_options)
    end
  end
end
