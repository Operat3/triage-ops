# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/database/database_processor'

RSpec.describe Triage::DatabaseProcessor do
  using RSpec::Parameterized::TableSyntax

  include_context 'with event', 'Triage::MergeRequestEvent'

  let(:instance) { described_class.new(event) }

  describe '#database_reviewed_or_approved?' do
    subject { instance.database_reviewed_or_approved? }

    context 'without the labels' do
      it { is_expected.to be(false) }
    end

    context 'with the "database::approved" label' do
      let(:label_names) { [Labels::DATABASE_APPROVED_LABEL] }

      it { is_expected.to be(true) }
    end

    context 'with the "database::reviewed" label' do
      let(:label_names) { [Labels::DATABASE_REVIEWED_LABEL] }

      it { is_expected.to be(true) }
    end
  end

  describe '#not_spam?' do
    subject { instance.not_spam? }

    context 'without the "Spam" label' do
      it { is_expected.to be(true) }
    end

    context 'with the "Spam" label' do
      let(:label_names) { [Labels::SPAM_LABEL] }

      it { is_expected.to be(false) }
    end
  end
end
