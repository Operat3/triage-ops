# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/copy_security_issue_labels'
require_relative '../../triage/triage/event'

RSpec.describe Triage::CopySecurityIssueLabels do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org_security?: true,
        description: "Default description + \n| Issue on [GitLab](https://gitlab.com/gitlab-org/gitlab/issues) | https://gitlab.com/gitlab-org/gitlab/-/issues/12345  |",
        project_path_with_namespace: described_class::SECURITY_GITLAB_PROJECT_PATH,
        project_id: 12345,
        iid: 6789
      }
    end

    let(:label_names) { ['security'] }
  end

  subject { described_class.new(event) }

  let(:main_issue) do
    {
      labels: ['security', 'vulnerability', 'breaking change',
        'severity::4', 'type::bug', 'group::optimize',
        'devops::manage', 'section::dev', 'backend', 'frontend']
    }
  end

  let(:unique_comment) { nil }

  before do
    stub_api_request(path: "/projects/gitlab-org%2Fgitlab/issues/12345", response_body: main_issue)
    # Unique comment check
    stub_api_request(
      path: "/projects/12345/issues/6789/notes",
      query: { per_page: 100 },
      response_body: [unique_comment].compact)
  end

  include_examples 'registers listeners', ['issue.open', 'issue.update']

  describe '#applicable?' do
    context 'when applicable' do
      include_examples 'event is applicable'
    end

    context 'when event project is not under gitlab-org/security' do
      before do
        allow(event).to receive(:from_gitlab_org_security?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not for security issue' do
      before do
        allow(event).to receive(:project_path_with_namespace).and_return('something-not-related')
      end

      include_examples 'event is not applicable'
    end

    context 'when event issue has no linked issue in main repo' do
      before do
        allow(event).to receive(:description).and_return('Description without main issue URL')
      end

      include_examples 'event is not applicable'
    end

    context 'when event issue has invalid link to main repo' do
      before do
        allow(event).to receive(:description).and_return("Default description + \n| Issue on [GitLab](https://gitlab.com/gitlab-org/gitlab/issues) | §§§§ |")
      end

      include_examples 'event is not applicable'
    end

    context 'when event issue has no issues to add' do
      let(:main_issue) { { labels: [] } }

      include_examples 'event is not applicable'
    end

    context 'when event issue has invalid link in main project' do
      before do
        stub_api_request(path: "/projects/gitlab-org%2Fgitlab/issues/12345", response_status: 404, response_body: { message: "404 Not found" })
      end

      include_examples 'event is not applicable'
    end

    context 'when unique comment already present' do
      let(:unique_comment) { { body: Triage::UniqueComment.new(described_class.name, event).wrap("Posted") } }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment with copying missing labels from main issue to security issue' do
      body = <<~MARKDOWN.chomp
        Copied ~"vulnerability" ~"breaking change" ~"severity::4" ~"type::bug" ~"group::optimize" ~"devops::manage" ~"section::dev" ~"backend" ~"frontend" from main project issue.
        /label ~"vulnerability" ~"breaking change" ~"severity::4" ~"type::bug" ~"group::optimize" ~"devops::manage" ~"section::dev" ~"backend" ~"frontend"
      MARKDOWN

      body = Triage::UniqueComment.new(described_class.name, event).wrap(body)
      body = add_automation_suffix('copy_security_issue_labels.rb', body)

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
