# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/versioned_milestone'
require_relative '../../lib/slo_breach_helper'

RSpec.describe SloBreachHelper do
  let(:resource_klass) do
    Struct.new(:labels, :milestone) do
      include SloBreachHelper
    end
  end

  let(:label_klass) { Struct.new(:name) }
  let(:labels) { [] }
  let(:milestone) { double }
  let(:versioned_milestone_double) { VersionedMilestone.new(subject) }

  subject { resource_klass.new(labels) }

  before do
    allow(VersionedMilestone).to receive(:new).and_return(versioned_milestone_double)
  end

  describe '#current_severity_label' do
    context 'with applied severity label' do
      let(:labels) { [label_klass.new(described_class::SEVERITY_LABELS[:s1])] }

      it 'returns correct string' do
        expect(subject.current_severity_label).to eq(described_class::SEVERITY_LABELS[:s1])
      end
    end

    context 'with no applied severity label' do
      it 'returns nil' do
        expect(subject.current_severity_label).to be_nil
      end
    end
  end

  context 'when added labels with dates' do
    let(:slo_breach_helper) do
      Class.new do
        attr_reader :labels, :milestone

        include SloBreachHelper

        def initialize(labels, milestone)
          @labels = labels
          @milestone = milestone
        end

        def labels_with_details; end
      end
    end

    let(:applied_severity) { described_class::SEVERITY_LABELS[:s1] }
    let(:labels) { [label_klass.new(applied_severity)] }
    let(:latest_severity_label) do
      double(name: applied_severity, added_at: label_added_at)
    end

    let(:label_added_at) { 35.days.ago }

    subject { slo_breach_helper.new(labels, milestone) }

    before do
      allow(subject).to receive(:labels_with_details).and_return([latest_severity_label])
    end

    describe '#slo_breach_looming?' do
      before do
        allow(versioned_milestone_double).to receive(:current?).and_return(false)
      end

      context 'when severity label applied and age makes slo breached' do
        it 'returns true' do
          expect(subject.slo_breach_looming?).to be(true)
        end
      end

      context 'when severity label applied and age makes slo warning' do
        let(:label_added_at) { 16.days.ago }

        it 'returns true' do
          expect(subject.slo_breach_looming?).to be(true)
        end
      end

      context 'when severity label applied and age young' do
        let(:label_added_at) { 15.days.ago }

        it 'returns false' do
          expect(subject.slo_breach_looming?).to be(false)
        end
      end

      context 'when for current milestone' do
        before do
          allow(versioned_milestone_double).to receive(:current?).and_return(true)
        end

        it 'returns false' do
          expect(subject.slo_breach_looming?).to be(false)
        end
      end
    end

    describe '#days_til_slo_breach_with_breach_date' do
      around do |example|
        Timecop.freeze(2021, 0o1, 31) { example.run }
      end

      context 'when severity label applied and age makes slo breached' do
        let(:label_added_at) { 35.days.ago }

        it 'has correct value' do
          expected = "-5 days (2021-01-26)"

          expect(subject.days_til_slo_breach_with_breach_date).to eq(expected)
        end
      end

      context 'when severity label applied and age makes slo warning' do
        let(:label_added_at) { 25.days.ago }

        it 'has correct value' do
          expected = "5 days (2021-02-05)"

          expect(subject.days_til_slo_breach_with_breach_date).to eq(expected)
        end
      end

      context 'when severity label applied and age young' do
        let(:label_added_at) { 5.days.ago }

        it 'has correct value' do
          expected = "25 days (2021-02-25)"

          expect(subject.days_til_slo_breach_with_breach_date).to eq(expected)
        end
      end
    end

    describe '#overdue?' do
      let(:milestone_klass) {  Struct.new(:due_date) }
      let(:milestone_due_date) { Date.parse('2021-01-17') }
      let(:milestone) { milestone_klass.new(milestone_due_date) }
      let(:label_added_at) { Date.parse('2021-01-01') }

      shared_examples 'SLO overdue issues' do |severity:, slo_target_in_days:|
        let(:applied_severity) { described_class::SEVERITY_LABELS[severity] }
        let(:slo_due_date) { label_added_at + slo_target_in_days }

        context 'when assigned milestone due date past SLO due date' do
          let(:milestone_due_date) { slo_due_date + 1 }

          it 'returns true' do
            expect(subject.overdue?).to be(true)
          end
        end

        context 'when assigned milestone due date falls on SLO due date' do
          let(:milestone_due_date) { slo_due_date }

          it 'returns false' do
            expect(subject.overdue?).to be(false)
          end
        end

        context 'when assigned milestone due date before SLO due date' do
          let(:milestone_due_date) { slo_due_date - 1 }

          it 'returns false' do
            expect(subject.overdue?).to be(false)
          end
        end
      end

      it_behaves_like 'SLO overdue issues', severity: :s1, slo_target_in_days: 30
      it_behaves_like 'SLO overdue issues', severity: :s2, slo_target_in_days: 60
      it_behaves_like 'SLO overdue issues', severity: :s3, slo_target_in_days: 90
      it_behaves_like 'SLO overdue issues', severity: :s4, slo_target_in_days: 120

      context 'when there is no severity label' do
        before do
          allow(subject).to receive(:labels_with_details).and_return([])
        end

        it 'returns false' do
          expect(subject.overdue?).to be(false)
        end
      end

      context 'when there is no milestone' do
        let(:milestone) { nil }

        it 'returns false' do
          expect(subject.overdue?).to be(false)
        end
      end

      context 'when milestone has no due date (e.g Backlog)' do
        let(:milestone_due_date) { nil }

        it 'returns false' do
          expect(subject.overdue?).to be(false)
        end
      end
    end

    describe '#milestones_count_before_breach_slo?' do
      around do |example|
        Timecop.freeze(2019, 12, 20) { example.run }
      end

      let(:today) { Date.today }
      let(:milestones) do
        [
          { title: '12.7', start_date: '2019-12-18', due_date: '2020-1-17' },
          { title: '12.8', start_date: '2020-1-18', due_date: '2020-2-17' },
          { title: '12.9', start_date: '2020-2-18', due_date: '2020-3-17' }
        ].map do |milestone_attributes|
          Gitlab::Triage::Resource::Milestone.new(milestone_attributes)
        end
      end

      let(:labels_with_details) do
        [double(name: severity, added_at: label_added_at)]
      end

      let(:labels) { labels_with_details }

      before do
        allow(versioned_milestone_double).to receive(:all_active_with_start_date).and_return(milestones)
        allow(subject).to receive(:labels_with_details).and_return(labels_with_details)
      end

      context 'with severity::1 label added 10 days ago' do
        let(:label_added_at) { 10.days.ago }
        let(:severity)       { 'severity::1' }

        it 'returns 1 because it breaches in 20 days' do
          expect(subject.milestones_count_before_breach_slo?).to eq(1)
          expect(subject.send(:breach_date) - today).to eq(20)
        end
      end

      context 'with severity::1 label added 90 days ago' do
        let(:label_added_at) { 90.days.ago }
        let(:severity) { 'severity::1' }

        it 'returns 0 milestone because it already breached 60 days ago' do
          expect(subject.milestones_count_before_breach_slo?).to eq(0)
          expect(subject.send(:breach_date) - today).to eq(-60)
        end
      end

      context 'with severity::1 label added 30 days ago' do
        let(:label_added_at) { 31.days.ago }
        let(:severity) { 'severity::1' }

        it 'returns 0 milestone because it already breached yesterday' do
          expect(subject.milestones_count_before_breach_slo?).to eq(0)
          expect(subject.send(:breach_date) - today).to eq(-1)
        end
      end

      context 'with severity::4 label' do
        let(:vulnerability_label) { double(name: 'FedRAMP Milestone::Vuln Remediation') }
        let(:severity_4_label) { double(name: 'severity::4', added_at: label_added_at) }
        let(:labels_with_details) { [severity_4_label] }

        context 'with severity label added 170 days ago' do
          let(:label_added_at) { 170.days.ago }

          context 'without vulnerability label' do
            it 'returns 0 because it breached 50 days ago' do
              expect(subject.milestones_count_before_breach_slo?).to eq(0)
              expect(subject.send(:breach_date) - today).to eq(-50)
            end
          end

          context 'with vulnerability label' do
            let(:labels_with_details) { [severity_4_label, vulnerability_label] }

            it 'returns 1 because it breaches in 10 days' do
              expect(subject.milestones_count_before_breach_slo?).to eq(1)
              expect(subject.send(:breach_date) - today).to eq(10)
            end
          end
        end

        context 'with severity label added 100 days ago' do
          let(:label_added_at) { 100.days.ago }

          context 'without vulnerability label' do
            it 'returns 1, and breaches in 20 days' do
              expect(subject.milestones_count_before_breach_slo?).to eq(1)
              expect(subject.send(:breach_date) - today).to eq(20)
            end
          end

          context 'with vulnerability label' do
            let(:labels_with_details) { [severity_4_label, vulnerability_label] }

            it 'returns 3 and breaches in 80 days' do
              expect(subject.milestones_count_before_breach_slo?).to eq(3)
              expect(subject.send(:breach_date) - today).to eq(80)
            end
          end
        end
      end
    end

    describe '#milestone_before_breach' do
      let(:labels_with_details) do
        [double(name: severity, added_at: label_added_at)]
      end

      let(:milestones) do
        [
          { title: '12.7', start_date: '2019-12-18', due_date: '2020-1-17' },
          { title: '12.8', start_date: '2020-1-18', due_date: '2020-2-17' },
          { title: '12.9', start_date: '2020-2-18', due_date: '2020-3-17' }
        ].map do |milestone_attributes|
          Gitlab::Triage::Resource::Milestone.new(milestone_attributes)
        end
      end

      before do
        allow(versioned_milestone_double).to receive(:all_active_with_start_date).and_return(milestones)
        allow(subject).to receive(:breach_date).and_return(breach_date)
      end

      context 'when breach date happens on the first day of a milestone' do
        let(:breach_date) { Date.parse('2020-1-18') }

        it 'returns the previous milestone' do
          expect(subject.milestone_before_breach.title).to eq('12.7')
        end
      end

      context 'when breach date happens in the middle of a milestone' do
        let(:breach_date) { Date.parse('2020-1-19') }

        it 'returns this milestone' do
          expect(subject.milestone_before_breach.title).to eq('12.8')
        end
      end

      context 'when breach date happens on the last day of a milestone' do
        let(:breach_date) { Date.parse('2020-2-17') }

        it 'returns this milestone' do
          expect(subject.milestone_before_breach.title).to eq('12.8')
        end
      end
    end
  end

  describe '#responsible_em' do
    let(:labels) { [label_klass.new('group::source code')] }

    context 'with available frontend and backend em' do
      context 'with backend label' do
        before do
          labels << label_klass.new('backend')
        end

        it 'returns the group backend em' do
          expect(subject.responsible_em).to eq(GroupDefinition.em_for_team('source_code'))
        end
      end

      context 'with frontend label' do
        before do
          labels << label_klass.new('frontend')
        end

        it 'returns the group frontend em' do
          expect(subject.responsible_em).to eq(GroupDefinition.em_for_team('source_code', :frontend))
        end
      end
    end

    context 'with no em available for the group' do
      before do
        allow(GroupDefinition).to receive(:em_for_team).and_return(nil)
      end

      it 'returns the group label' do
        expect(subject.responsible_em).to eq('~"group::source code" Engineering Manager')
      end
    end
  end

  describe '#responsible_pm' do
    let(:labels) { [label_klass.new('group::source code')] }

    context 'with pm available for the group' do
      it 'returns the group pm' do
        expect(subject.responsible_pm).to eq(GroupDefinition.pm_for_team('source_code'))
      end
    end

    context 'with no pm available for the group' do
      before do
        allow(GroupDefinition).to receive(:pm_for_team).and_return(nil)
      end

      it 'returns the group label' do
        expect(subject.responsible_pm).to eq('~"group::source code" Product Manager')
      end
    end
  end

  describe '#responsible_set' do
    let(:labels) { [label_klass.new('group::source code')] }
    let(:expected_set) { '@set' }

    before do
      allow(subject).to receive(:current_group_set).and_return(expected_set)
    end

    context 'with set available for the group' do
      it 'returns the group set' do
        expect(subject.responsible_set).to eq(expected_set)
      end
    end

    context 'with no set available for the group' do
      let(:expected_set) { nil }

      it 'returns the group label' do
        expect(subject.responsible_set).to eq('~"group::source code" Software Engineer in Test')
      end
    end
  end

  describe '#community_team_escalation' do
    let(:labels) { [label_klass.new('group::source code')] }

    context 'with backend label' do
      before do
        labels << label_klass.new('backend')
      end

      it 'returns the corresponding group backend em' do
        expect(subject.community_team_escalation)
          .to eq(GroupDefinition.em_for_team('source_code'))
      end
    end

    context 'with frontend label' do
      before do
        labels << label_klass.new('frontend')
      end

      it 'returns the corresponding group frontend em' do
        expect(subject.community_team_escalation)
          .to eq(GroupDefinition.em_for_team('source_code', :frontend))
      end
    end

    context 'without group label' do
      let(:labels) { [] }

      it 'returns the community backup DRI' do
        expect(subject.community_team_escalation)
          .to eq(described_class::COMMUNITY_BACKUP_DRI)
      end
    end
  end

  describe '#current_group_set' do
    let(:labels) { [label_klass.new('group::source code')] }

    context "when there is a SET counterpart" do
      before do
        allow(subject).to receive(:current_group_set).and_return("@set")
      end

      it 'returns the SET username' do
        expect(subject.current_group_set).to eq("@set")
      end
    end

    context "when there is no SET counterpart" do
      before do
        allow(subject).to receive(:current_group_set).and_return(nil)
      end

      it 'returns nil' do
        expect(subject.current_group_set).to be_nil
      end
    end
  end

  describe '#markdown_label' do
    it 'returns label string with escaped quotes' do
      expect(subject.markdown_label('some label')).to eq("~\"some label\"")
    end
  end
end
