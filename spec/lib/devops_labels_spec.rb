# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/devops_labels'

RSpec.describe DevopsLabels, :http_requests_allowed do
  before do
    stub_request(:get, "https://about.gitlab.com/sections.json")
      .to_return(status: 200, body: read_fixture('sections.json'), headers: {})

    stub_request(:get, "https://about.gitlab.com/stages.json")
      .to_return(status: 200, body: read_fixture('stages.json'), headers: {})

    stub_request(:get, "https://about.gitlab.com/groups.json")
      .to_return(status: 200, body: read_fixture('groups.json'), headers: {})

    stub_request(:get, "https://about.gitlab.com/categories.json")
      .to_return(status: 200, body: read_fixture('categories.json'), headers: {})
  end

  describe '.stages' do
    it 'returns the stages' do
      stages = described_class.stages

      expect(stages).to include('manage', 'systems')
    end
  end

  describe '.groups' do
    it 'returns the groups' do
      groups = described_class.groups

      expect(groups).to include('authentication_and_authorization', 'compliance')
    end
  end

  describe '.category_labels' do
    it 'returns the category labels' do
      labels = described_class.category_labels

      expect(labels).to include('Category:Subgroups', 'static analysis')
    end
  end

  describe '.department_labels' do
    it 'returns the department labels' do
      labels = described_class.department_labels

      expect(labels.first).to eq('Quality')
    end
  end

  describe '.department?' do
    where(:department) do
      [
        'Quality',
        'Engineering Productivity',
        'Contributor Success',
        'Technical Writing',
        'infrastructure'
      ]
    end

    with_them do
      it 'returns true when given a department' do
        expect(described_class.department?(department)).to be(true)
      end
    end

    it 'returns true when given a department' do
      expect(described_class.department?('Foo')).to be(false)
    end
  end

  describe '.teams_per_user' do
    where(:username, :expected_teams) do
      [
        ['andr3', %w[code_review source_code]],
        ['egrieff', ['product_planning']],
        ['luke', []],
        ['gweaver', ['dev']],
        ['gonzoyumo', ['composition_analysis']],
        ['lulalala', ['provision']],
        ['non-gitlab-user', []]
      ]
    end

    with_them do
      it 'returns teams of user' do
        expect(described_class.teams_per_user(username)).to contain_exactly(*expected_teams)
      end
    end
  end

  describe DevopsLabels::Context do
    let(:resource_klass) do
      Struct.new(:labels) do
        include DevopsLabels::Context
      end
    end

    let(:label_klass) do
      Struct.new(:name)
    end

    let(:resource) { resource_klass.new([]) }

    describe '#label_names' do
      it 'returns [] if the resource has no label' do
        resource = resource_klass.new([])

        expect(resource.label_names).to eq([])
      end

      it 'returns the label names if the resource has labels' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.department_labels.first)])

        expect(resource.label_names).to eq([DevopsLabels.department_labels.first])
      end
    end

    describe '#current_section_label' do
      it 'returns nil if the resource has no section label' do
        resource = resource_klass.new([])

        expect(resource.current_section_label).to be_nil
      end

      it 'returns the section label if the resource has a section label' do
        section_label = WwwGitLabCom.sections.first[1]['label']
        resource = resource_klass.new([label_klass.new(section_label)])

        expect(resource.current_section_label).to eq(section_label)
      end

      it 'returns the section label if the resource has an unknown section label' do
        section_label = "#{DevopsLabels::SECTION_LABEL_PREFIX}foo bar baz"
        resource = resource_klass.new([label_klass.new(section_label)])

        expect(resource.current_section_label).to eq(section_label)
      end

      it 'returns nil if the resource has a nested "sub-section" label' do
        resource = resource_klass.new([label_klass.new("#{WwwGitLabCom.sections.first[1]['label']}::nested label")])

        expect(resource.current_section_label).to be_nil
      end

      it 'returns the section label if the resource has a section label and a nested "sub-section" label' do
        first_section_label = WwwGitLabCom.sections.to_a[0][1]['label']
        second_section_label = WwwGitLabCom.sections.to_a[1][1]['label']
        resource = resource_klass.new([label_klass.new("#{first_section_label}::nested label"), label_klass.new(second_section_label)])

        expect(resource.current_section_label).to eq(second_section_label)
      end
    end

    describe '#current_stage_label' do
      it 'returns nil if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.current_stage_label).to be_nil
      end

      it 'returns the stage label if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.department_labels.first)])

        expect(resource.current_stage_label).to eq(DevopsLabels.department_labels.first)
      end

      it 'returns the stage label if the resource has an unknown stage label' do
        stage_label = "#{DevopsLabels::STAGE_LABEL_PREFIX}foo bar baz"
        resource = resource_klass.new([label_klass.new(stage_label)])

        expect(resource.current_stage_label).to eq(stage_label)
      end

      it 'returns nil if the resource has a nested "sub-stage" label' do
        resource = resource_klass.new([label_klass.new("#{DevopsLabels::STAGE_LABEL_PREFIX}#{DevopsLabels.stages.first}::nested label")])

        expect(resource.current_stage_label).to be_nil
      end

      it 'returns the department label if the resource has a department label' do
        resource = resource_klass.new([label_klass.new('Quality')])

        expect(resource.current_stage_label).to eq('Quality')
      end

      it 'returns the stage label if the resource has a department label and a stage label' do
        stage_label = "#{DevopsLabels::STAGE_LABEL_PREFIX}create"
        resource = resource_klass.new([label_klass.new('Quality'), label_klass.new(stage_label)])

        expect(resource.current_stage_label).to eq(stage_label)
      end

      it 'returns the stage label if the resource has a stage label and a nested "sub-stage" label' do
        first_stage_label = DevopsLabels.department_labels[0]
        second_stage_label = DevopsLabels.department_labels[1]
        resource = resource_klass.new([label_klass.new("#{first_stage_label}::nested label"), label_klass.new(second_stage_label)])

        expect(resource.current_stage_label).to eq(second_stage_label)
      end
    end

    describe '#current_group_label' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.current_group_label).to be_nil
      end

      it 'returns the group label if the resource has a group label' do
        group_label = "#{DevopsLabels::GROUP_LABEL_PREFIX}#{DevopsLabels.groups.first}"
        resource = resource_klass.new([label_klass.new(group_label)])

        expect(resource.current_group_label).to eq(group_label)
      end

      it 'returns the group label if the resource has an unknown group label' do
        group_label = "#{DevopsLabels::GROUP_LABEL_PREFIX}foo bar baz"
        resource = resource_klass.new([label_klass.new(group_label)])

        expect(resource.current_group_label).to eq(group_label)
      end

      it 'returns nil if the resource has a nested "sub-group" label' do
        resource = resource_klass.new([label_klass.new("#{DevopsLabels::GROUP_LABEL_PREFIX}#{DevopsLabels.groups.first}::nested label")])

        expect(resource.current_group_label).to be_nil
      end
    end

    describe '#current_infrastructure_team_label' do
      it 'returns nil if the resource has no infrastructure team label' do
        resource = resource_klass.new([])

        expect(resource.current_infrastructure_team_label).to be_nil
      end

      it 'returns the group label if the resource has an infrastructure team label' do
        resource = resource_klass.new([label_klass.new('team::Scalability')])

        expect(resource.current_infrastructure_team_label).to eq('team::Scalability')
      end
    end

    describe '#current_department_label' do
      it 'returns nil if the resource has no department label' do
        resource = resource_klass.new([])

        expect(resource.current_department_label).to be_nil
      end

      it 'returns the department label if the resource has a department label' do
        resource = resource_klass.new([label_klass.new('Quality')])

        expect(resource.current_department_label).to eq('Quality')
      end
    end

    describe '#current_category_labels' do
      it 'returns [] if the resource has no category label' do
        resource = resource_klass.new([])

        expect(resource.current_category_labels).to eq([])
      end

      it 'returns the category labels if the resource has a category label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.category_labels.first)])

        expect(resource.current_category_labels).to eq([DevopsLabels.category_labels.first])
      end
    end

    describe '#current_workflow_label' do
      it 'returns nil if the resource has no workflow label' do
        resource = resource_klass.new([])

        expect(resource.current_workflow_label).to be_nil
      end

      it 'returns the workflow label if the resource has a workflow label' do
        resource = resource_klass.new([label_klass.new('workflow::verification')])

        expect(resource.current_workflow_label).to eq('workflow::verification')
      end
    end

    describe '#current_stage_name' do
      it 'returns nil if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.current_stage_name).to be_nil
      end

      it 'returns the stage name if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new("#{DevopsLabels::STAGE_LABEL_PREFIX}#{DevopsLabels.stages.first}")])

        expect(resource.current_stage_name).to eq(DevopsLabels.stages.first)
      end

      it 'returns nil if the resource has a nested "sub-stage" label' do
        resource = resource_klass.new([label_klass.new('devops::release::merge trains')])

        expect(resource.current_stage_name).to be_nil
      end

      it 'returns the stage name if the resource has a stage label and a nested "sub-stage" label' do
        first_stage_label = "#{DevopsLabels::STAGE_LABEL_PREFIX}#{DevopsLabels.stages[0]}"
        second_stage_label = "#{DevopsLabels::STAGE_LABEL_PREFIX}#{DevopsLabels.stages[1]}"
        second_stage = DevopsLabels.stages[1]
        resource = resource_klass.new([label_klass.new("#{first_stage_label}::merge trains"), label_klass.new(second_stage_label)])

        expect(resource.current_stage_name).to eq(second_stage)
      end
    end

    describe '#current_group_name' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.current_group_name).to be_nil
      end

      it 'returns the group name if the resource has a group label' do
        resource = resource_klass.new([label_klass.new("#{DevopsLabels::GROUP_LABEL_PREFIX}#{DevopsLabels.groups.first.tr('_', ' ')}")])

        expect(resource.current_group_name).to eq(DevopsLabels.groups.first)
      end
    end

    describe '#current_group_name_with_spaces' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.current_group_name_with_spaces).to be_nil
      end

      it 'returns the group name if the resource has a group label' do
        resource = resource_klass.new([label_klass.new("#{DevopsLabels::GROUP_LABEL_PREFIX}#{DevopsLabels.groups.first.tr('_', ' ')}")])

        expect(resource.current_group_name_with_spaces).to eq(DevopsLabels.groups.first.tr('_', ' '))
      end
    end

    describe '#all_category_labels_for_stage' do
      it 'returns [] if the stage does not exists' do
        expect(resource.all_category_labels_for_stage(nil)).to eq([])
        expect(resource.all_category_labels_for_stage('')).to eq([])
        expect(resource.all_category_labels_for_stage('foo')).to eq([])
      end

      it 'returns the stage category labels when given a stage name' do
        expect(resource.all_category_labels_for_stage('release')).to include('Category:Continuous Delivery', 'Category:Release Evidence')
        expect(resource.all_category_labels_for_stage('release')).not_to include('logging')
      end
    end

    describe '#all_category_labels_for_group' do
      it 'returns [] if the group does not exists' do
        expect(resource.all_category_labels_for_stage(nil)).to eq([])
        expect(resource.all_category_labels_for_stage('')).to eq([])
        expect(resource.all_category_labels_for_stage('foo')).to eq([])
      end

      it 'returns the stage category labels when given a group name' do
        resource = resource_klass.new([])

        expect(resource.all_category_labels_for_group('release')).to include('Category:Continuous Delivery')
        expect(resource.all_category_labels_for_group('release')).not_to include('release governance')
      end
    end

    describe '#section_for_stage' do
      it 'returns the section for the specific stage' do
        resource = resource_klass.new([])

        expect(resource.section_for_stage('create')).to eq('dev')
        expect(resource.section_for_stage('verify')).to eq('ops')
      end
    end

    describe '#section_for_group' do
      it 'returns the section for the specific group' do
        resource = resource_klass.new([])

        expect(resource.section_for_group('source_code')).to eq('dev')
        expect(resource.section_for_group('application_performance')).to eq('enablement')
      end
    end

    describe '#has_department_label?' do
      it 'returns false if the resource has no department label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_department_label
      end

      it 'returns true if the resource has a department label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.department_labels.first)])

        expect(resource).to be_has_department_label
      end
    end

    describe '#has_stage_label?' do
      it 'returns false if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_stage_label
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.department_labels.first)])

        expect(resource).to be_has_stage_label
      end

      it 'returns false if the resource has a nested "sub-stage" label' do
        resource = resource_klass.new([label_klass.new('devops::release::merge trains')])

        expect(resource).not_to be_has_stage_label
      end
    end

    describe '#has_group_label?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_group_label
      end

      it 'returns false if the resource is labeled with group::not_owned [DEPRECATED]' do
        resource = resource_klass.new([label_klass.new('group::not_owned [DEPRECATED]')])

        expect(resource).not_to have_group_label
      end

      it 'returns true if the resource has a group label' do
        resource = resource_klass.new([label_klass.new("#{DevopsLabels::GROUP_LABEL_PREFIX}#{DevopsLabels.groups.first}")])

        expect(resource).to have_group_label
      end
    end

    describe '#can_add_default_group?' do
      it 'returns true if the resource has no labels' do
        resource = resource_klass.new([])

        expect(resource).to be_can_add_default_group
      end

      it 'returns false if the resource has an group label' do
        resource = resource_klass.new([label_klass.new('group::access')])

        expect(resource).not_to be_can_add_default_group
      end

      it 'returns false if the resource has an infrastructure team label' do
        resource = resource_klass.new([label_klass.new('team::Scalability')])

        expect(resource).not_to be_can_add_default_group
      end

      it 'returns false if the resource has a department label' do
        resource = resource_klass.new([label_klass.new('Quality')])

        expect(resource).not_to be_can_add_default_group
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new('devops::manage')])

        expect(resource).to be_can_add_default_group
      end
    end

    describe '#can_add_default_group_and_stage?' do
      it 'returns true if the resource has no labels' do
        resource = resource_klass.new([])

        expect(resource).to be_can_add_default_group_and_stage
      end

      it 'returns false if the resource has a group label' do
        resource = resource_klass.new([label_klass.new('group::access')])

        expect(resource).not_to be_can_add_default_group_and_stage
      end

      it 'returns false if the resource has an infrastructure team label' do
        resource = resource_klass.new([label_klass.new('team::Scalability')])

        expect(resource).not_to be_can_add_default_group_and_stage
      end

      it 'returns false if the resource has a department label' do
        resource = resource_klass.new([label_klass.new('Quality')])

        expect(resource).not_to be_can_add_default_group_and_stage
      end

      it 'returns false if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new('devops::manage')])

        expect(resource).not_to be_can_add_default_group_and_stage
      end
    end

    describe '#has_category_label?' do
      it 'returns false if the resource has no category label' do
        resource = resource_klass.new([])

        expect(resource).not_to have_category_label
      end

      it 'returns true if the resource has a category label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.category_labels.first)])

        expect(resource).to have_category_label
      end
    end

    describe '#has_category_label_for_current_stage?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_category_label_for_current_stage
      end

      it 'returns true if the resource has a stage label and a corresponding category label' do
        resource = resource_klass.new([label_klass.new('devops::release'), label_klass.new('Category:Continuous Delivery')])

        expect(resource).to be_has_category_label_for_current_stage
      end

      it 'returns false if the resource has a stage label but no corresponding category label' do
        resource = resource_klass.new([label_klass.new('devops::release'), label_klass.new('logging')])

        expect(resource).not_to be_has_category_label_for_current_stage
      end

      it 'returns true if the resource has a stage label, a corresponding category label, and an unrelated nested "sub-stage" label' do
        resource = resource_klass.new([label_klass.new('devops::create::web ide'), label_klass.new('devops::create'), label_klass.new('Category:Web IDE')])

        expect(resource).to be_has_category_label_for_current_stage
      end
    end

    describe '#has_category_label_for_current_group?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_category_label_for_current_group
      end

      it 'returns true if the resource has a group label and a corresponding category label' do
        resource = resource_klass.new([label_klass.new('group::release'), label_klass.new('Category:Continuous Delivery')])

        expect(resource).to be_has_category_label_for_current_group
      end

      it 'returns false if the resource has a group label but no corresponding category label' do
        resource = resource_klass.new([label_klass.new('group::release'), label_klass.new('release governance')])

        expect(resource).not_to be_has_category_label_for_current_group
      end
    end

    describe '#can_infer_labels?' do
      let(:section_label) do
        label_klass.new(WwwGitLabCom.sections.first[1]['label'])
      end

      let(:stage_label) do
        label_klass.new(WwwGitLabCom.stages.first[1]['label'])
      end

      let(:group_label) do
        label_klass.new(WwwGitLabCom.groups.first[1]['label'])
      end

      let(:category_label) do
        label_klass.new(
          DevopsLabels.category_labels_per_group.dig(DevopsLabels.groups.first, 0))
      end

      let(:category_label_from_other_group) do
        label_klass.new(
          DevopsLabels.category_labels_per_group.dig(DevopsLabels.groups.last, 0))
      end

      it 'returns false if the resource has no stage, group, category, or team label' do
        resource = resource_klass.new([])

        expect(resource.can_infer_labels?).to be(false)
      end

      it 'returns false if it has all labels we may infer' do
        resource = resource_klass.new([
          stage_label, group_label, category_label, section_label
        ])

        expect(resource.can_infer_labels?).to be(false)
      end

      it 'returns true if it has all labels except category label for the particular group' do
        resource = resource_klass.new([
          stage_label, group_label, category_label_from_other_group, section_label
        ])

        expect(resource.can_infer_labels?).to be(true)
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([stage_label])

        expect(resource.can_infer_labels?).to be(true)
      end

      it 'returns true if the resource has a group label' do
        resource = resource_klass.new([group_label])

        expect(resource.can_infer_labels?).to be(true)
      end

      it 'returns true if the resource has a category label' do
        resource = resource_klass.new([category_label])

        expect(resource.can_infer_labels?).to be(true)
      end
    end

    describe '#can_infer_group_from_author?' do
      let(:resource_klass) do
        Struct.new(:author) do
          include DevopsLabels::Context
        end
      end

      let(:author) { 'johndoe' }
      let(:resource) { resource_klass.new(author) }

      before do
        allow(resource).to receive(:group_for_user).with(author)
      end

      it 'returns true if there is a matching group for user' do
        allow(resource).to receive(:group_for_user).and_return(true)

        expect(resource.can_infer_group_from_author?).to be(true)
      end

      it 'returns false if there is no matching group for user' do
        allow(resource).to receive(:group_for_user).and_return(nil)

        expect(resource.can_infer_group_from_author?).to be(false)
      end
    end

    describe '#stage_has_a_single_group?' do
      it 'returns false if the stage does not exists' do
        expect(resource.stage_has_a_single_group?(nil)).to be(false)
        expect(resource.stage_has_a_single_group?('')).to be(false)
        expect(resource.stage_has_a_single_group?('foo')).to be(false)
      end

      it 'returns true if the give stage has a single group' do
        expect(resource.stage_has_a_single_group?('configure')).to be(true)
      end

      it 'returns false if the resource has several team labels' do
        expect(resource.stage_has_a_single_group?('create')).to be(false)
      end
    end

    describe '#group_part_of_stage?' do
      let(:stage_label) { 'devops::create' }
      let(:group_label) { 'group::source code' }
      let(:category_label) { 'Category:Code Review' }
      let(:current_labels) { [stage_label, group_label] }
      let(:resource) { resource_klass.new(current_labels.map { |l| label_klass.new(l) }) }

      context 'when stage and group do not exist' do
        let(:current_labels) { [] }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when stage does not exist' do
        let(:current_labels) { [group_label] }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when group does not exist' do
        let(:current_labels) { [stage_label] }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when group is not part of stage' do
        let(:stage_label) { 'devops::configure' }
        let(:group_label) { 'group::pipeline execution' }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when group is part of stage' do
        it 'returns true' do
          expect(resource.group_part_of_stage?).to be(true)
        end
      end
    end

    describe '#first_group_for_stage' do
      it 'returns false if the stage does not exists' do
        expect(resource.first_group_for_stage(nil)).to be_nil
        expect(resource.first_group_for_stage('')).to be_nil
        expect(resource.first_group_for_stage('foo')).to be_nil
      end

      it 'returns the first group of the given stage' do
        expect(resource.first_group_for_stage('growth')).to eq('acquisition')
        expect(resource.first_group_for_stage('anti-abuse')).to eq('anti-abuse')
      end
    end

    describe '#stage_for_group' do
      it 'returns nil if the no stage corresponds to the given group' do
        expect(resource.stage_for_group(nil)).to be_nil
        expect(resource.stage_for_group('')).to be_nil
        expect(resource.stage_for_group('foo')).to be_nil
      end

      it 'returns the stage name of the given group' do
        expect(resource.stage_for_group('acquisition')).to eq('growth')
        expect(resource.stage_for_group('anti-abuse')).to eq('anti-abuse')
      end
    end

    describe '#comment_for_intelligent_stage_and_group_labels_inference' do
      where(:case_name, :current_labels, :expected_new_stage_and_group_labels_from_intelligent_inference, :explanation) do
        [
          ["stage: yes, group: yes, category: yes, team: no => Only section.", ["devops::configure", "group::configure", "Category:Auto DevOps"], ["section::ops"], ''],

          ["stage: yes, group: yes, category: no, team: no => Only section.", ["devops::configure", "group::configure"], ["section::ops"], ''],
          ["stage: yes, group: yes (doesn't exist in stages.yml), category: no, team: no => No inference", ["section::ops", "devops::monitor", "group::foo bar baz"], [], ''],

          ["stage: yes, group: no, category: yes, team: no (100% matching stage) => Group based on feature since feature matches stage", ["devops::create", "Category:Source Code Management"], ["group::source code", "section::dev"], %(Setting label(s) ~"group::source code" ~"section::dev" based on ~"Category:Source Code Management" ~"group::source code".)],
          ["stage: yes, group: no, category: yes, team: no (100% matching stage, 2 different groups) => Manual triage required", ["devops::create", "Category:Source Code Management", "merge requests"], ["section::dev"], ''],
          ["stage: yes, group: no, category: yes, team: no (66% matching stage) => Group based on feature since feature matches stage", ["devops::create", "Category:Code Review Workflow", "Category:GitLab CLI", "analytics"], ["group::code review", "section::dev"], %(Setting label(s) ~"group::code review" ~"section::dev" based on ~"Category:Code Review Workflow" ~"Category:GitLab CLI" ~"group::code review".)],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage) => Group based on feature since feature matches stage", ["devops::create", "Category:Source Code Management", "analytics"], ["group::source code", "section::dev"], %(Setting label(s) ~"group::source code" ~"section::dev" based on ~"Category:Source Code Management" ~"group::source code".)],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage) => Group based on stage since feature does not match stage and stage has only one group", ["devops::configure", "Category:Design Management", "analytics"], ["group::configure", "section::ops"], %(Setting label(s) ~"group::configure" ~"section::ops" based on ~"devops::configure" ~"group::configure".)],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Group based on feature since feature matches stage", ["devops::create", "Category:Source Code Management", "analytics", "epics"], ["group::source code", "section::dev"], %(Setting label(s) ~"group::source code" ~"section::dev" based on ~"Category:Source Code Management" ~"group::source code".)],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Manual triage required since feature is less than 50%", ["devops::create", "search", "analytics", "epics"], ["section::dev"], ''],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Group based on stage since feature does not match stage and stage has only one group", ["devops::configure", "Category:Design Management", "analytics", "epics"], ["group::configure", "section::ops"], %(Setting label(s) ~"group::configure" ~"section::ops" based on ~"devops::configure" ~"group::configure".)],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Manual triage required since feature does not match stage but stage has several groups", ["devops::secure", "Category:Design Management"], ["section::sec"], ''],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Group based on stage since feature does not match stage and stage has only one group", ["devops::configure", "Category:Design Management"], ["group::configure", "section::ops"], %(Setting label(s) ~"group::configure" ~"section::ops" based on ~"devops::configure" ~"group::configure".)],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Group based on category since group has only one category", ["devops::data_stores", "Category:Global Search"], ["group::global search", "section::enablement"], %(Setting label(s) ~"group::global search" ~"section::enablement" based on ~"Category:Global Search" ~"group::global search".)],

          ["stage: yes, group: no, category: no, team: no => Manual triage required", ["devops::verify"], ["section::ops"], ''],

          ["stage: no, group: yes, category: yes, team: no => Stage based on group", ["Category:Source Code Management", "group::source code", "markdown"], ["devops::create", "section::dev"], %(Setting label(s) ~"devops::create" ~"section::dev" based on ~"group::source code".)],

          ["stage: no, group: yes, category: no, team: no => Stage label based on group", ["Category:Application Performance", "group::application_performance"], ["devops::data_stores", "section::enablement"], %(Setting label(s) ~"devops::data_stores" ~"section::enablement" based on ~"group::application_performance".)],

          ["stage: no, group: no, category: no, team: no => Manual triage required", ["bug", "rake tasks"], [], ''],

          ["stage: no, group: no, category: yes, team: no (best match: 100%) => Stage and group based on feature", ["Category:Internationalization"], ["devops::manage", "group::import", "section::dev"], %(Setting label(s) ~"devops::manage" ~"group::import" ~"section::dev" based on ~"Category:Internationalization" ~"group::import".)],
          ["stage: no, group: no, category: yes, team: no (best match: 100%) => Stage and group based on category", ["Category:Source Code Management"], ["devops::create", "group::source code", "section::dev"], %(Setting label(s) ~"devops::create" ~"group::source code" ~"section::dev" based on ~"Category:Source Code Management" ~"group::source code".)],
          ["stage: no, group: no, category: yes, team: no (best match: 66%) => Manual triage required", ["Category:Snippets", "Category:Web IDE", "Category:Internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: no (best match: 50%) => Manual triage required", ["Category:Wiki", "Category:Web IDE", "Category:Importers", "Category:Internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: no (best match: 33%) => Manual triage required", ["pipeline", "elasticsearch", "Category:Internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: no (no match) => Manual triage required", ["backstage"], [], ''],

          ["~QA => ~Quality", ["QA"], ["Quality"], %(Setting label(s) ~"Quality" based on ~"QA".)],
          ["~Quality ~QA => No new labels", %w[Quality QA], [], ''],
          ["~Quality ~group::package => Only section", ["Quality", "group::package registry"], ["section::ops"], ''],

          ["~static analysis => ~Engineering Productivity", ["static analysis"], ["Engineering Productivity"], %(Setting label(s) ~"Engineering Productivity" based on ~"static analysis".)],
          ["~Engineering Productivity ~static analysis => No new labels", ["Engineering Productivity", "static analysis"], [], ''],

          ["stage: yes, group: yes, category: no, single_category: no => Apply Section label based on group, cannot infer Category based on group", ["group::global search", "devops::data_stores"], ["section::enablement"], %(Setting label(s) ~"section::enablement" based on ~"group::global search".)],
          ["stage: yes, group: yes (not part of the stage), category: no, single_category: no => No new labels", ["group::pipeline insights", "devops::create"], [], ''],
          ["stage: no, group: yes, category: no, single_category: no => Apply Stage based on group, cannot infer Category based on group", ["group::geo"], ["devops::systems", "section::enablement"], %(Setting label(s) ~"devops::systems" ~"section::enablement" based on ~"group::geo".)],
          ["stage: yes, group: yes, category: no, single_category: no => Stage and Group already applied, cannot infer Category based on group. Do nothing", ["group::geo", "devops::systems"], ["section::enablement"], ''],
          ["stage: no, group: yes, category: no => Apply Stage and Category labels ", ["group::product analytics"], ["Category:Product Analytics", "devops::analytics", "section::analytics"], %(Setting label(s) ~"Category:Product Analytics" ~"devops::analytics" ~"section::analytics" based on ~"group::product analytics".)],

          ["stage: no, group: yes, category: yes => Apply stage and group labels to triage report", ["triage report", "Category:Source Code Management"], ["devops::create", "group::source code", "section::dev"], %(Setting label(s) ~"devops::create" ~"group::source code" ~"section::dev" based on ~"Category:Source Code Management" ~"group::source code".)]
        ]
      end

      it "returns nil if the resource does not warrant any new labels" do
        resource = resource_klass.new([])

        expect(resource.comment_for_intelligent_stage_and_group_labels_inference).to be_nil
      end

      with_them do
        it "returns a comment with a /label quick action" do
          resource = resource_klass.new(current_labels.map { |l| label_klass.new(l) })
          label_action = "/label #{expected_new_stage_and_group_labels_from_intelligent_inference.map { |l| %(~"#{l}") }.join(' ')}"

          if expected_new_stage_and_group_labels_from_intelligent_inference.empty?
            expect(resource.comment_for_intelligent_stage_and_group_labels_inference).to be_nil
          elsif expected_new_stage_and_group_labels_from_intelligent_inference.all? { |label| label.start_with?('section::') }
            # When there's only section labels added, be quiet without explanation
            expect(resource.comment_for_intelligent_stage_and_group_labels_inference).to eq(label_action)
          else
            expect(resource.comment_for_intelligent_stage_and_group_labels_inference).to eq %(#{explanation}\n#{label_action})
          end
        end
      end
    end

    describe '#group_for_user' do
      it 'returns nil when given bad username' do
        expect(resource.group_for_user(nil)).to be_nil
        expect(resource.group_for_user('')).to be_nil
      end

      it 'returns the group name of the matching group for user' do
        expect(resource.group_for_user('gonzoyumo')).to eq('composition_analysis')
      end

      it 'returns nil when user has multiple groups' do
        expect(resource.group_for_user('djensen')).to be_nil
      end

      it 'returns nil when team refers to stage' do
        expect(resource.group_for_user('jramsay')).to be_nil
      end
    end

    describe '#comment_for_mr_author_group_label' do
      let(:resource_klass) do
        Struct.new(:author) do
          include DevopsLabels::Context
        end
      end

      let(:resource) { resource_klass.new(author) }

      context 'when author belongs to one group' do
        let(:author) { 'gonzoyumo' }

        it 'returns comment with a /label quick action' do
          expected_label = '~"group::composition analysis"'
          expected_comment = "Setting label #{expected_label} based on `@#{author}`'s group.\n/label #{expected_label}"

          expect(resource.comment_for_mr_author_group_label).to eq(expected_comment)
        end
      end

      context 'when author belongs to multiple groups' do
        let(:author) { 'djensen' }

        it 'returns nil' do
          expect(resource.comment_for_mr_author_group_label).to be_nil
        end
      end
    end
  end
end
