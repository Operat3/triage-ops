# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/devops_labels_nudger_job'
require_relative '../../triage/triage/event'

RSpec.describe Triage::DevopsLabelsNudgerJob do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:event_attrs) do
      { project_id: Triage::Event::GITLAB_PROJECT_ID,
        event_actor_username: 'username',
        label_names: label_names }
    end
  end

  let(:label_names)              { ['group::global search'] }
  let(:issue_notes_api_path)     { "/projects/#{project_id}/issues/#{iid}/notes" }

  let(:missing_devops_labels_comment_request_body) do
    <<~MARKDOWN.chomp
      <!-- triage-serverless DevopsLabelsNudgerJob--DevopsLabelsMissing -->
      :wave: @username, #{described_class::LABELS_MISSING_MESSAGE}
    MARKDOWN
  end

  subject { described_class.new }

  describe '#perform' do
    context 'when issue_path does not require any label change' do
      before do
        allow(subject).to receive(:applicable?).and_return(false)
      end

      it 'does nothing' do
        expect_no_request(
          verb: :post,
          request_body: { body: missing_devops_labels_comment_request_body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end

    context 'when issue require label change' do
      before do
        allow(subject).to receive(:applicable?).and_return(true)
      end

      it 'pings user name' do
        expect_api_request(
          verb: :post,
          request_body: { body: missing_devops_labels_comment_request_body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end

    context 'with labels_set' do
      before do
        allow(subject).to receive(:labels_set?).and_return(true)
        allow(subject).to receive(:previous_discussion?).and_return(false)
        allow(event).to receive(:by_team_member?).and_return(true)
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(true)
      end

      it 'does nothing' do
        expect_no_request(
          verb: :post,
          request_body: { body: missing_devops_labels_comment_request_body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end

    context 'with a previous comment' do
      before do
        allow(subject).to receive(:labels_set?).and_return(false)
        allow(subject).to receive(:previous_discussion?).and_return(true)
        allow(event).to receive(:by_team_member?).and_return(true)
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(true)
      end

      it 'does nothing' do
        expect_no_request(
          verb: :post,
          request_body: { body: missing_devops_labels_comment_request_body },
          path: issue_notes_api_path,
          response_body: {}
        ) do
          subject.perform(event)
        end
      end

      context 'with a different project' do
        before do
          allow(subject).to receive(:labels_set?).and_return(false)
          allow(subject).to receive(:previous_discussion?).and_return(false)
          allow(event).to receive(:by_team_member?).and_return(true)
          allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
        end

        it 'does nothing' do
          expect_no_request(
            verb: :post,
            request_body: { body: missing_devops_labels_comment_request_body },
            path: issue_notes_api_path,
            response_body: {}
          ) do
            subject.perform(event)
          end
        end

        context 'with a different user' do
          before do
            allow(subject).to receive(:labels_set?).and_return(false)
            allow(subject).to receive(:previous_discussion?).and_return(false)
            allow(event).to receive(:by_team_member?).and_return(false)
            allow(event).to receive(:from_gitlab_org_gitlab?).and_return(true)
          end

          it 'does nothing' do
            expect_no_request(
              verb: :post,
              request_body: { body: missing_devops_labels_comment_request_body },
              path: issue_notes_api_path,
              response_body: {}
            ) do
              subject.perform(event)
            end
          end
        end
      end
    end
  end
end
